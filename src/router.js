import VueRouter from "vue-router";
import Vue from "vue";
import Start from "./components/Start";
import Questions from "./components/Questions";
import Results from "./components/Results";

Vue.use(VueRouter)

const routes = [
    {
        path: "/start",
        alias: "/",
        component: Start
    },
    {
        path: "/questions",
        component: Questions
    },
    {
        path: "/results",
        component: Results
    }
]


export default new VueRouter({ routes })