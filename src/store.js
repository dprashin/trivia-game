import Vuex from 'vuex'
import Vue from "vue";
import axios from "axios";
import {shuffleAnswers, prepareAnswers} from "./api/arrayMods"

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        user: {},
        questionTypes: [],
        categories: [],
        questions: [],
        quizAnswers: [],
        correctAnswers: [],
        userAnswers: [],
        numberOfQuestions: [
            {questions: "5", ID: 0},
            {questions: "10", ID: 1},
            {questions: "15", ID: 2}
        ],
        userName: '',
        difficulties: [
            {difficulty: "easy", ID: 0},
            {difficulty: "medium", ID: 1},
            {difficulty: "hard", ID: 2},
        ],
        userScore: 0,
        userCreated: false,
        selectedDifficulty: "",
        selectedCategory: 0,
        selectedNumberOfQuestions: "",
        currentQuestion: 0

    },
    getters: {},
    mutations: {
        incrementCurrentQuestion(state, payload) {
            state.currentQuestion += payload;
        },
        setUser(state, payload) {
            state.user = payload;
        },
        incrementScore(state, payload) {
            state.userScore += payload;
        },
        saveAnswer(state, payload) {
            state.userAnswers.push(payload)
        },
        setUsername(state, payload) {
            state.userName = payload;
        },
        loadCategories(state, payload) {
            state.categories = payload.trivia_categories;
        },
        loadQuestions(state, payload) {
            state.questions = payload.results;
        },
        setCategory: (state, payload) => {
            state.selectedCategory = payload;
        },
        setNumberOfQuestions: (state, payload) => {
            state.selectedNumberOfQuestions = payload;
        },
        setDifficulty: (state, payload) => {
            state.selectedDifficulty = payload;
        },
        getUserByUserName(state, payload) {
            state.user = payload.results;
            console.log(payload)
        },
        populateQuiz(state, payload) {
            state.questions = [];
            state.quizAnswers = [];
            state.userScore = 0;
            state.question_types = [];
            state.currentQuestion = 0;
            state.correctAnswers = [];
            state.userAnswers = []
            state.userScore = 0

            payload.results.map((e) => {
                state.questionTypes.push(e.type);

                //Replace all apostrophes
                const regex = new RegExp("&#039;", "g");
                e.question = e.question.replace(regex, "'");

                //replace all quotes
                const regex2 = new RegExp("&quot;", "g");
                e.question = e.question.replace(regex2, '"');

                state.questions.push(e.question);

                state.correctAnswers.push(e.correct_answer)
            });

            state.quizAnswers = shuffleAnswers(prepareAnswers(payload));
            console.log(state.quizAnswers)
        }
    },
    actions: {
        async createUser({commit}, username) {
            const user = await axios({
                method: 'post',
                url: 'https://dd-dp-n-api.herokuapp.com/trivia/',
                headers: {'x-api-key': 'dd-dp-2021'},
                data: {
                    user: { username: username, highScore: 0 }
                }
            }).then(r => r.data);
            commit("setUser", user)
        },
        async updateUser({state}, userName, userScore){
            await axios({
                method: 'PATCH',
                url: `https://dd-dp-n-api.herokuapp.com/trivia/trivia/${state.user.id}`,
                headers: {'x-api-key': 'dd-dp-2021'},
                data: {
                    user: { username: userName, highScore: userScore }
                }
            })
        },
        sendAnswer(state , value) {
            if (value.validity == true) {
                //increment score
                this.commit("incrementScore", 1);
            }
            //increment current question
            this.commit("incrementCurrentQuestion", 1);

            //save answer
            this.commit("saveAnswer", value.answer)

        },
        loadCategories({commit}) {
            axios("https://opentdb.com/api_category.php").then((response) => {
                commit("loadCategories", response.data);
            });
        },
        loadQuestions({state, commit}) {
            const numberOfQuestions = state.selectedNumberOfQuestions
            const difficulty = state.selectedDifficulty
            const category = state.selectedCategory;

            axios(`https://opentdb.com/api.php?amount=${numberOfQuestions}&category=${category}&difficulty=${difficulty}`)
                .then((response) => {
                    commit("populateQuiz", response.data);
                })
        },
        async getUserByUsername({state, commit, dispatch}) {
            const username = state.userName;
            console.log(username);
            const user = await axios(`https://dd-dp-n-api.herokuapp.com/trivia?username=${username}`)
                .then((response) => response.data)
            if (user.length === 0) {
                dispatch("createUser", username)
            } else {
                commit("setUsername", user.username)
            }
        },
        async updateUserScore({state, dispatch}) {
            // state.user && Object.keys(state.user).length !== 0 &&
            if(state.user.highScore < state.userScore){
                dispatch("updateUser", state.userName, state.userScore)
            }
        }
    }
})