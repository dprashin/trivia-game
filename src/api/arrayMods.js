//This function takes in an array, of arrays and shuffles each
//individual array an array of arrays, with internally shuffled elements. s
export function shuffleAnswers(array) {

    let output=[]

    //Put as many empty arrays in our output array as we have in our
    array.map(()=>{
        output.push([])
    })

    for (let index = 0; index < array.length; index++) {
        array[index].map((ee) => {
            if (Math.random() > 0.5) {
                output[index].push(ee);
            } else {
                output[index].unshift(ee);
            }
        });
    }

    return output;
}

//This function merges two arrays from the JSON object. Incorrect answers and correct answers then it creates an array of arrays of objects.
//The objects contain the answer, and whether it's a true or a false answer. This is so they can easily be validated even after the array of arrays,
//has been shuffled internally by the shuffle function.
export function prepareAnswers(array) {
    let newArray = [[]];
    let counter = 0;
    array.results.map((element) => {
        element.incorrect_answers.map((e) => {
            newArray[counter].push({ answer: e, validity: false });
        });
        newArray[counter].push({
            answer: element.correct_answer,
            validity: true,
        });
        counter++;
        newArray.push([]);
    });
    newArray.pop();

    return newArray;
}